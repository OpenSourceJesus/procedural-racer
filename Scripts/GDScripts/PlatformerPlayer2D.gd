class PlatformerPlayer2D:
	extends KinematicBody2D
	
	export var moveSpeed = 1
	export var gravity = 50
	export onready var player = get_node("Player")
	export onready var trs = get_transform()
	var velocity = Vector2.ZERO;

	func _ready():
		pass
	
	func _process(delta):
		if Input.is_action_pressed("Move Left"):
			Move(-1, delta)
		if Input.is_action_pressed("Move Right"):
			Move(1, delta)
	
	func Move(move, delta):
		if not test_move(get_transform(), Vector2.RIGHT * move * moveSpeed * delta):
			position.x = move * moveSpeed * delta
	
	func Gravity(move, delta):
		if not test_move(get_transform(), Vector2.DOWN * moveSpeed * delta):
			velocity.y += Vector2.DOWN * moveSpeed * delta

	func _physics_process(delta):
		pass
	#	var result = RayCast2D(global_position, $Pixel.scale, self, collision_mask)
	#	if result:
	#		print("Hit at point", result.position)
